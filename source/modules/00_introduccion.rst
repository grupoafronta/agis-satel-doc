#######################
aGIS AST Introducción
#######################

La documentación adjunta describe la interfaz y uso de la plataforma aGIS AST.

aGIS AST es una plataforma WebGIS, orientada a la gestión y planificación de Redes de Telecomunicaciones, especialmente redes de Fibra Óptica en cualquiera de sus ámbitos de aplicación (FTTx, Capacidad...), aunque también contempla la integración con redes eléctricas, redes de canalizaciones y obra civil, redes de radio, redes de coaxial así como las combinaciones híbridas de todas.

La plataforma se presenta en una interfaz focalizada en un mapa Web, mediante en el que una serie de capas modelan los distintos elementos de una red, permitiendo el dibujo, representación y conexión de los elementos.


El conjunto de módulos de aGIS AST permiten dotar al sistema de funcionalidades avanzadas que posibilitan la mejora de experiencia para el usuario de cara a la planificación y toma de decisiones. Entre los módulos más destacados: módulo de inventario de red, módulo de exportación de datos...

aGIS apuesta por un modelo de datos **abierto**, pues todas las capas y documentación generada pueden exportarse de aGIS e importarse y reprocesarse en cualquier software GIS horizontal.

