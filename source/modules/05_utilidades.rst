##########
Utilidades
##########

En este menú, encontramos una serie de herramientas que complementan las utilidades de aGIS ya descritas en capitulos anteriores.

.. figure::  images/05_utilidades/image1.png
   :align:   center
   :width: 30%


Inventario
**********

Esta herramienta permite visualizar los equipos de red después de pulsar sobre “CALCULAR”. Los datos que devuelve están divididos por capas y son independientes.Se puede exportar los datos en formato CSV, PDF o copiarlos directamente al portapapeles.

.. figure::  images/05_utilidades/image2.png
   :align:   center
   :width: 80%

.. figure::  images/05_utilidades/image3.png
   :align:   center
   :width: 80%


Gestor de Exportación
*********************

Esta herramienta permite la exportación tanto por selección como de forma general, para ello, vamos a Utilidades -> Exportación

El display que se muestra en pantalla es sencillo y de fácil comprensión. Se muestra una zona superior en la que se muestra un mapa. En la parte inferior, se encuentra la zona donde elegimos los datos a exportar.

Para exportar los datos de forma general, se debe seleccionar una capa y el formato en el que se quiere exportar y, por último, pulsar sobre EXPORTAR.

.. figure::  images/05_utilidades/image4.png
   :align:   center
   :width: 80%

.. |image5| image:: images/05_utilidades/image5.png
    :width: 40

Si deseamos exportar solo una zona del mapa, se puede seleccionar con la herramienta de polígonos |image5| , la sección deseada:

.. figure::  images/05_utilidades/image6.png
   :align:   center
   :width: 80%

Posteriormente, elegimos la capa, el formato y le damos al botón EXPORTAR.


Gestor de Trabajos
******************

En el gestor de trabajo, se pueden observar y administrar los distintos trabajos que se han de realizar sobre la red. Para crear un nuevo trabajo, se ha de pulsar sobre el botón "Nuevo Trabajo" y rellenar el formulario que se solicita:

**Nombre:**
    Nombre que se le desea poner al trabajo.

**Descripción:**
    En caso de desear introducir alguna descripción u observación al trabajo.

**Estado:**
    Situación en la que se encuentra el trabajo.

**Capa:**
    Capa en la cual se encuentra el elemento que queremos seleccionar.

**Elemento:**
    Elemento en cuestión sobre el que ha de realizarse el trabajo.

**Activar notificaciones:**
    De manera predeterminada, el que genera el trabajo recibe notificaciones, pero adicionalmente, si marcamos esta casilla y rellenamos con otro correo, también éste recibirá las notificaciones.

**Mail:**
    Dirección de email.

Para guardar el trabajo, se ha de hacer clic sobre guardar.

.. figure::  images/05_utilidades/image7.png
   :align:   center
   :width: 80%

   Formulario Trabajos

.. figure::  images/05_utilidades/image8.png
   :align:   center
   :width: 80%

   Tabla Trabajos


Como vemos en la segunda captura, al guardar nos aparece una tabla donde se muestran todos los trabajos con información relevante. Si hacemos clic en alguno de los  que aparece en la última columna de la fila podremos:

.. |image10| image:: images/05_utilidades/image10.png
       :width: 25
.. |image11| image:: images/05_utilidades/image11.png
       :width: 25
.. |image12| image:: images/05_utilidades/image12.png
       :width: 25

**Visualizar** |image10|
    Nos muestra tres columnas en las que se encuentra de izquierda a derecha: Detalles, Actividad y Documentos. Las dos últimas columnas nos permiten añadir tanto la resolución a la incidencia en modo de texto como asignar documentación sobre la misma.

.. figure::  images/05_utilidades/image9.png
   :align:   center
   :width: 80%

**Editar** |image11|
    Permite editar los datos del trabajo.

**Eliminar** |image12|
    Permite borrar el trabajo.


Gestor de Incidencias
*********************

El gestor de incidencias permite administrar las incidencias relacionadas con la red, exportarlas a diferentes formatos o imprimirlas. Para crear una nueva incidencia, hay que hacer clic sobre "Nueva Incidencia" y rellenar el formulario que se solicita.

**Nombre:**
    Nombre que se le desea poner a la incidencia.

**Descripción:**
    En caso de desear introducir alguna descripción u observación a la incidencia.

**Estado:**
    Situación en la que se encuentra la incidencia.

**Capa:**
    Capa en la cual se encuentra el elemento que queremos seleccionar.

**Elemento:**
    Elemento en cuestión sobre el que está la incidencia.

**Activar notificaciones:**
    De manera predeterminada, el que genera la incidencia recibe notificaciones, pero adicionalmente, si marcamos esta casilla y rellenamos con otro correo, también éste recibirá las notificaciones.

**Mail:**
    Dirección de email.

Para guardar la incidencia, se ha de hacer clic sobre "guardar".


.. figure::  images/05_utilidades/image13.png
   :align:   center
   :width: 80%

   Formulario Incidencias

.. figure::  images/05_utilidades/image14.png
   :align:   center
   :width: 80%

   Tabla Incidencias

Como vemos en la segunda captura, al guardar nos aparece una tabla donde se muestran todas las incidencias con información relevante. Si hacemos clic en el círculo que aparece en la primera columna de la fila que deseemos podremos:

**Visualizar** |image10|
    Nos muestra tres columnas en las que se encuentra de izquierda a derecha: Detalles, Actividad y Documentos. Las dos últimas columnas nos permiten añadir tanto la resolución a la incidencia en modo de texto como asignar documentación sobre la misma.

.. figure::  images/05_utilidades/image15.png
   :align:   center
   :width: 80%

**Editar** |image11|
    Permite editar los datos del trabajo.

**Eliminar** |image12|
    Permite borrar el trabajo.


Gestor Documental
*****************

Con esta herramienta, podemos subir archivos de distinta índole a la plataforma, permitiendo exportar los ya existentes en formato CSV, XLS, PDF o incluso imprimirlo. A la hora de crear un nuevo documento, debemos darle al botón "Nuevo Documento" y rellenar un formulario que se compone de:

**Nombre:** Nombre que se le quiere poner al documento en aGIS.

**Capa:**
    Capa en la cual se encuentra el elemento que queremos seleccionar.

**Elemento:**
    Elemento en cuestión sobre el que está la incidencia.

**Archivo:** El documento en cuestión.

.. figure::  images/05_utilidades/image16.png
   :align:   center
   :width: 80%

Una vez guardamos, tenemos varias opciones: ver el archivo, pulsando sobre "Ver" o borrarlo con |image12|.

.. figure::  images/05_utilidades/image17.png
   :align:   center
   :width: 80%

