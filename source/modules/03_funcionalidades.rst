########################
Funcionalidades del Mapa
########################

El acceso al mapa principal (núcleo o core de la aplicación) es muy sencillo y accesible gracias al menú lateral que nos permite, desde cualquier lugar dentro de la aplicación, acceder clicando en el botón "Mapa"




Una vez dentro, podemos comprobar que se despliegan más opciones que examinaremos más adelante.


Mapa
****

.. figure::  images/03_funcionalidades/image1.png
   :align:   center
   :width: 40%

Se permite visualizar y acceder a las distintas herramientas y utilidades que permiten interactuar con todo el contenido de la plataforma. La plataforma se compone, principalmente, de dos componentes, un Mapa Base, el cual se puede intercambiar entre los que dispone la plataforma, y un conjunto de capas que se sobreponen al Mapa Base.


Mapa base
=========
Una vez dentro del mapa existe una serie de herramientas básicas:

**1. Copiar coordenadas**

Mediante este botón que se encuentra en la parte superior derecha, podemos seleccionar un punto en el mapa y que nos diga sus coordenadas, tanto en metros como en grados (si pulsamos sobre “Latitud” nos cambiarán las unidades de medida). Simplemente, hacemos clic en un punto del mapa y nos aparecerá arriba a la derecha. Una vez hecho, clicamos en el botón "Copiar Coord" y se nos copiará en el portapapeles.

.. figure::  images/03_funcionalidades/image2.png
   :align:   center
   :width: 50%


.. figure::  images/03_funcionalidades/image3.png
   :align:   center
   :width: 50%

**2. Gestor de impresión**

Mediante este botón, que se encuentra en la parte superior derecha, encontramos los diferentes tipos de impresión.

.. figure::  images/03_funcionalidades/image200.png
   :align:   center
   :width: 30%

.. |image201| image:: images/03_funcionalidades/image201.png
    :width: 25

.. |image202| image:: images/03_funcionalidades/image202.png
    :width: 25

.. |image203| image:: images/03_funcionalidades/image203.png
    :width: 25

|image201| Imprime lo que se visualiza en la pantalla

|image202| Imprime lo que se visualiza en la pantalla horizontalmente

|image203| Imprime lo que se visualiza en la pantalla verticalmente


**3. Zoom**

Zoom nos permite acercar o alejar la vista del mapa. También está la opción de hacerlo con la rueda del ratón.

.. figure::  images/03_funcionalidades/image8.png
   :align:   center

**4. Pantalla completa**

Nos muestra la vista del mapa a pantalla completa.

.. figure::  images/03_funcionalidades/image165.png
   :align:   center


**5. Selector mapa base**

La plataforma dispone de distintos Mapas Base con los que poder trabajar como referencia. Para cambiar los Mapas Base, se debe acceder al botón en forma de mapa del mundo.

.. figure::  images/03_funcionalidades/image95.png
   :align:   center

Actualmente, al cambiar de mapa base, se borran y reinician literalmente todas las capas no vectoriales, añadiendo el basemap nuevo. Esto optimiza el visionado y evita problemas.

Cuando se accede al menú, aparece una nueva ventana con todos los mapas disponibles y una previsualización de los mismos.

.. figure::  images/03_funcionalidades/image7.png
   :align:   center
   :width: 80%

La carga de estos mapas es dinámica, es decir, podremos encontrar más o menos mapas según el proyecto o fecha. Algunos de los mapas por defecto son los siguientes:

* ESRI Carreteras:
    Recurso público de ESRI que muestra la red de carreteras a nivel Global.

* Google Satélite Hybrid:
    Ortofoto Satélite pública de Google, con etiquetado.

* Google Satélite:
    Ortofoto Satélite pública de Google, sin etiquetado.

* Esri Satélite:
    Recurso público de ESRI que muestra ortofoto a nivel Global.

* CARTO Light:
    Recurso de CARTO que provee un mapa de carga sencilla, con líneas sencillas, muy recomendable para una visualización básica.

* IGN PNOA:
    Ortofotos de máxima actualidad del IGN.

* IGN Catastro:
    Recurso público del IGN (Instituto Geográfico Nacional) de catastros nacionales.

* IGN Catastro B/N:
    Recurso público del IGN (Instituto Geográfico Nacional) de catastros nacionales en blanco y negro.

* OSM:
    Recurso público y colaborativo de callejeros y catastros. Muy recomendado.

**6.Medir distancia y áreas**

Esta herramienta nos permite medir, tanto distancias de un punto a otro, como el perímetro o el área.

.. figure::  images/03_funcionalidades/image111.png
   :align:   center

Para medir, hacemos clic en la herramienta y seleccionamos los puntos en el mapa haciendo un clic.

.. figure::  images/03_funcionalidades/image41.png
   :align:   center
   :width: 30%

Para medir el perímetro o el área, vamos seleccionando los puntos que conforman el polígono y, cuando vayamos a cerrarlo, hacemos doble clic.

.. figure::  images/03_funcionalidades/image55.png
   :align:   center
   :width: 30%

**7. Escala**

Esta herramienta se encuentra situada en la esquina inferior izquierda y nos muestra una escala que va cambiando según la ampliación del mapa.

.. figure::  images/03_funcionalidades/image116.png
   :align:   center


**8. Deshacer/avanzar vista**

Esta herramienta guarda el historial de la posición del mouse por cada click de movimiento en el mapa (vista), dándonos la posibilidad tanto de volver a vistas anteriores como de avanzar a la última realizada.

Posee un tercer botón (home) en el cual se vuelve a la vista general del mapa.

.. figure::  images/03_funcionalidades/image212.png
   :align:   center


Búsqueda de Red
===============

La pestaña de Búsqueda en Red permite localizar elementos por el campo nombre o bien, en caso de tenerlo, por el campo nombre alternativo. Esta función actúa para capas puntuales y lineales. Las capas, donde se desean hacer las búsquedas las proporcionan la herramienta mediante un menú checkbox.

.. figure::  images/03_funcionalidades/image4.png
   :align:   center
   :width: 30%

Filtros
=======

La pestaña de filtros ayuda al usuario a la hora de encontrar un objeto en específico. Se puede filtrar por:


* Capas Dorsales

.. figure::  images/03_funcionalidades/image5.png
   :align:   center
   :width: 30%

Podemos seleccionar los distintos elementos que se encuentran en la capa Dorsales.


* Cables

Dividida en tres opciones filtrados combinables entre si --> Estado, Capacidad o Cubierta

.. figure::  images/03_funcionalidades/image6.png
   :align:   center
   :width: 30%

.. figure::  images/03_funcionalidades/image9.png
   :align:   center
   :width: 30%

Podemos seleccionar los distintos elementos que se encuentran en la capa Cables.


* Canalizaciones

Dividida en dos opciones filtrados combinables entre si --> Estado y Tipo

.. figure::  images/03_funcionalidades/image10.png
   :align:   center
   :width: 30%

.. figure::  images/03_funcionalidades/image11.png
   :align:   center
   :width: 30%

Podemos seleccionar los distintos elementos que se encuentran en la capa Canalizaciones.


* Capas Auxiliares

.. figure::  images/03_funcionalidades/image12.png
   :align:   center
   :width: 30%

Podemos seleccionar entre los distintos tipos de capas auxiliares que se encuentran disponibles.


* Otras Auxiliares
    (Sin información por el momento)


Mi Ubicación
============

Esta herramienta aparece en la parte izquierda de la pantalla y solo será visible cuando estemos en el menú de Mapa.

Al clicar sobre Mi Ubicación, se colocará sobre la ubicación del usuario un marcador y se centrará el mapa sobre esa ubicación ofreciendo sus coordenadas.

.. figure::  images/03_funcionalidades/image96.png
   :align:   center
   :width: 30%


Capas
=====

aGIS, como Sistema de Información Geográfica, modela los elementos  mediante un sistema de capas que permite la flexibilidad y escalabilidad del sistema.

Sobre el mapa base, se visualizarán estas capas con los distintos elementos, atendiendo a un sistema de visualización.

Las capas son los distintos elementos que se observan a la hora de visualizar la red. Las Capas pueden ser puntuales, lineales o poligonales. También hay un conjunto de capas no geométricas que complementan a las Capas geométricas.

**1. Cables**
----------------
    Capa geométrica y lineal que representa el cableado de red de telecomunicaciones (fibra óptica, cobre, coaxial…).

**2. Fibras**
-------------------
    Capa no geométrica, heredada de cable, que representa los circuitos físicos dentro de un determinado cable (hilo de fibra, hilo de cobre, radioenlace…).

**3. Cocas**
------------
    Capa geométrica, y puntual, que representa las cocas existentes, una coca es aquel cable adicional en forma de espiral, bucle o rollo que se deja antes de una conexión, de forma que resulta flexible para absorber dilataciones.

**4. Equipos**
--------------
    Capa geométrica y puntual que representa el equipamiento de la red.

**5. Subequipos**
-----------------
    Capa no geométrica, heredada de equipos, que permite representar patch pannels, bandejas, y splitters dentro de un equipo.

**6. Terminales**
-----------------
    Capa no geométrica, heredada de terminales, que permite representar cualquier terminal de patch pannel, bandeja o splitter conectable.

**7. Estructuras**
------------------
    Capa geométrica, y puntual, que representa infraestructura puntual como cámaras, arquetas, postes...

**8. Canalizaciones**
---------------------
    Capa geométrica y lineal que representa infraestructura lineal como canalización, zanja, pasos aéreos...

**9. Conductos**
----------------
    Capa no geométrica, heredada de canalización para representar conductos dentro de arquetas.

**10.Subconductos**
-------------------
    Capa no geométrica, heredada de conducto para representar subconductos dentro de conductos.

**11. Nodos**
-------------
    Capa geométrica y poligonal para representar emplazamientos y nodos de la red.

**12. Asociaciones**
--------------------
    Capa no geométrica que permite asociar cables a canalizaciones mediante el parámetro uuid.

**13. Conexiones**
------------------
    Capa no geométrica que establece enlaces 1 a 1 entre pares terminal-terminal, terminal-fibra, fibra-terminal, y que contiene toda la información de conexionado de la red.

**14. Servicios**
-------------------
    Capa no geométrica  para almacenar información de los servicios.


Diseño
******

.. figure::  images/03_funcionalidades/image13.png
   :align:   center
   :width: 40%

Gestor de Modelos
=================

Es una de las herramientas principales de aGIS, ya que nos permite dibujar en el mapa los distintos elementos disponibles y asignarles los campos necesarios.

La interfaz del gestor de modelos se divide en tres partes:


**Selector de elemento:** Nos permite elegir el elemento que deseemos dibujar en el mapa.

.. figure::  images/03_funcionalidades/image14.png
   :align:   center
   :width: 40%


**Datos generales:** Formulario que dependiendo del elemento seleccionado varía sus campos para una mayor precisión.


**Datos Específicos:** Formulario opcional que permite que realicemos un seguimiento temporal del elemento, incluso con la opción de añadir observaciones. Contiene estos campos:

    * Fechas de última modificación
    * Fecha de Instalación
    * Fecha de Alta
    * Fecha de Baja
    * Observaciones

.. figure::  images/03_funcionalidades/image15.png
   :align:   center
   :width: 35%

Una vez rellenados los campos necesarios, pulsamos en el botón Dibujar para situar el objeto en el mapa. En el caso de capas lineales o poligonales, para finalizar el trazado, debemos hacer doble clic en el último punto que realicemos.

.. figure::  images/03_funcionalidades/image16.png
   :align:   center
   :width: 35%

1. Cables
---------

.. figure::  images/03_funcionalidades/image17.png
   :align:   center
   :width: 30%

**DATOS GENERALES:**
            **Nombre:**
                Nombre del cable.

            **Tipo:**
                Los tipos de cable disponibles son los siguientes:

                * Fibra G.655
                * Fibra G.652D
                * Fibra Mixto

            **Capacidad:**
                La cantidad de fibras ópticas que tendrá el cable.

            **Situación:**
                Los siguientes tipos de situaciones indica en qué medio se encuentra el cable.

                * Canalizado
                * Fachada
                * Interior
                * Poste
                * Zanja

            **Cubierta:**
                La cubierta es la parte exterior del cable preparada para proteger el interior del mismo de los agentes externos. Las cubiertas de las mangueras de Fibra Óptica, dependiendo de la aplicación o lugar de instalación, pueden ser:

                * PESP
                * KT
                * TKT
                * PKP
                * PKCP

            **Fabricante:**
                Nombre del fabricante.

            **Propietario:**
                El dueño del cableado.

            **Estado:**
                Indicar en qué estado se encuentra la instalación.

                * Proyectado
                * En construcción
                * Instalado

2. Canalizaciones
-----------------

.. figure::  images/03_funcionalidades/image18.png
   :align:   center
   :width: 30%

**DATOS GENERALES:**

            **Nombre:**
                Nombre de la canalización.

            **Tipo:**
                Indica cómo se realizará la canalización.

                * Canalización
                * Zanja
                * Paso Aéreo

            **Estado:**
                Indicar en qué estado se encuentra la instalación.

                * Proyectado
                * En construcción
                * Instalado
                * Operativo

            **Propietario:**
                Dueño de la canalización

            **Modalidad:**
                * Alquilado
                * Propiedad

            **Dimensión X**

            **Dimensión Y**

            **Anclajes:**
                Número de Anclajes en Canalización o paso Aéreo

            **Obturadores Totales:**
                Número de Obturadores Totales en la Canalización.

3. Cocas
--------

.. figure::  images/03_funcionalidades/image19.png
   :align:   center
   :width: 30%

**DATOS GENERALES:**
            **Nombre:**
                Nombre de la coca.

            **Referencia Cable:**
                Cable al que pertenece la COCA.

            **Longitud Inicial:**
                Longitud inicial de la canalización.
            **Longitud Final:**
                Longitud final de la canalización.

            **Estado:**
                Indicar en qué estado se encuentra la instalación.

                * Proyectado
                * En construcción
                * Instalado
                * Operativo


4. Equipos
----------

.. figure::  images/03_funcionalidades/image20.png
   :align:   center
   :width: 30%

.. figure::  images/03_funcionalidades/image23.png
   :align:   center
   :width: 30%

**DATOS GENERALES:**

            **Nombre:**
                Nombre del equipo.

            **Estado:**

                * Proyectado
                * En construcción
                * Instalado
                * Operativo


            **Tipo:**
                * Repartidor OMX
                * CA
                * CD
                * T
                * CE
                * P2P

            **Situación:**
                * ARQUETA
                * CAMARA
                * FACHADA
                * INTERIOR
                * PEDESTAL
                * POSTEL
                * REGISTRO

            **Acometidas:**
                Número de acometidas.

            **Arrastre (Div MAX)**



            **BANDEJAS:**
                - Nº de Bandejas
                - Capacidad

            **SPLITTERS:**
                - Nº de Splitters
                - Entradas
                - Salidas

            **PATCH PANNELS:**
                - Nº de Patch Pannels
                - Capacidad


5. Nodos
--------

.. figure::  images/03_funcionalidades/image21.png
   :align:   center
   :width: 30%

**DATOS GENERALES:**

            **Nombre:**
                Nombre del Nodo.

            **Código:**
                Número del código.

            **Tipo**

                * Nodo A
                * Nodo B
                * Nodo C
                * Nodo D

            **Dirección**

            **Estado:**

                * Proyectado
                * En construcción
                * Instalado
                * Operativo


6. Estructura
-------------

.. figure::  images/03_funcionalidades/image22.png
   :align:   center
   :width: 30%

**DATOS GENERALES:**

            **Nombre:**
                Nombre de la Estructura.

            **Tipo:**
                * Arqueta
                * Cámara de registro
                * Poster
                * Riser
                * Perforación

            **Material:**
                * Hormigón
                * En madera
                * Fundición

            **Propietario:**
                Nombre del propietario

            **Modalidad:**

                * Alquilado
                * En propiedad

            **Estado:**

                * Proyectado
                * En construcción
                * Instalado
                * Operativo


Ventanas de Información
=======================

Las ventanas de información se abrirán en la parte derecha de nuestra pantalla cuando se clica en los distintos elementos que se tengan representados en el mapa. Las ventanas de información varían según el elemento que se clica y según los servicios disponibles. En resumen, las ventanas de información desglosan de forma resumida toda la información del objeto que se haya seleccionado.


Mediante las ventanas de información, podremos terminar de configurar los distintos elementos, ver sus esquemas, ver documentos relacionados, obervaciones y modificar los datos que se introdujeron cuando se crearon.


.. figure::  images/03_funcionalidades/image24.png
   :align:   center
   :width: 30%

Como podemos observar en la imagen las interfaces están compuestas  por:

    **Nombre del elemento:**
        Es el nombre que establecemos al crear el elemento.


    **Elemento:**
        Tipo de elemento.


    **Datos Generales:**
        Contiene los datos generales de creación de un tipo de elemento en concreto.


    **Esquema:**
        Nos proporciona diferentes esquemas.


    **Datos Específicos:**
        Contiene datos concretos sobre la creación (fechas y observaciones).


    **Documentos:**
        Nos muestra los diferentes documentos asignados al elemento desde la utilidad Gestor Documental.


    **Observaciones:**
        Nos muestra las diferentes incidencias asignadas al elemento.


Además de estas pestañas, disponemos de tres botones en la parte superior derecha.


.. |image145| image:: images/03_funcionalidades/image145.png
    :width: 40

.. |image184| image:: images/03_funcionalidades/image184.png
    :width: 40


**Modificar** |image145|

    Para modificar un elemento, se debe presionar sobre el icono del lápiz y editar los datos generales o específicos. Desde esta ventana, podemos eliminar un elemento pulsando el botón eliminar.  Dependiendo del elemento, se habilitarán diferentes opciones concretas que analizaremos más adelante.


.. |image25| image:: images/03_funcionalidades/image25.png
    :width: 30

**Expandir/minimizar** |image25|

    Permite la minimización y expansión de la ventana de información.


**Cerrar** |image184|

    Mediante este botón podemos cerrar la ventana de información.


Vamos a analizar las opciones más concretas que poseen los distintos elementos:

a. Modificación del trazado
---------------------------
Los cables, las canalizaciones y líneas permiten la modificación de su trazado con la opción de Modificar.

Para ello, debemos estar en el modo de edición de uno de los elementos antes mencionados.


.. figure::  images/03_funcionalidades/image26.png
   :align:   center
   :width: 80%

Como vemos en la captura el trazado aparece en azul y se marcan un cuadrado en cada extremo y uno central.

Desde los extremos, podemos extender, encoger y mover la posición del punto.

.. figure::  images/03_funcionalidades/image27.png
   :align:   center
   :width: 80%

Como vemos en la imagen, hemos desplazado el trazado del cable.

Desde el cuadrado central, podemos añadir otro vértice al trazado.

.. figure::  images/03_funcionalidades/image28.png
   :align:   center
   :width: 80%

Entre cada vértice, se creará un cuadrado intermedio para poder editar.


b. Canalizaciones
-----------------

Desde la ventana de canalizaciones, podemos añadir conductos y subconductos de una manera sencilla.


.. figure::  images/03_funcionalidades/image29.png
   :align:   center
   :width: 40%


La canalización vacía se representa de la siguiente forma:

.. figure::  images/03_funcionalidades/image113.png
   :align:   center
   :width: 30%


.. |image38| image:: images/03_funcionalidades/image38.png
    :width: 80

**i. Añadir conducto**
    Para ello, desplegamos la pestaña conductos y subconductos. En la parte de conductos, seleccionamos las opciones que deseemos y le damos al botón |image38|

    Una vez hecho esto, nos aparecerá a la derecha de Conductos COND 1, si lo seleccionamos

.. figure::  images/03_funcionalidades/image66.png
   :align:   center
   :width: 30%

   Ahí aparecerán los conductos que vayamos creando.


.. figure::  images/03_funcionalidades/image99.png
   :align:   center
   :width: 30%

   Representación de un conducto


.. |image135| image:: images/03_funcionalidades/image135.png
    :width: 80


**ii. Eliminar conducto**
    Para eliminar un conducto, tan solo debemos seleccionarlo haciendo clic en el COND que queramos eliminar y darle al botón |image135|.


.. |image39| image:: images/03_funcionalidades/image39.png
    :width: 100

**iii. Añadir subconducto**
    Para añadir un subconducto, primero, debemos seleccionar un conducto. Una vez seleccionado el COND, elegimos las opciones que deseemos y le damos al botón |image39|.


    Una vez hecho, esto nos aparecerá a la derecha de Subconductos SUBCOND 1

.. figure::  images/03_funcionalidades/image255.png
   :align:   center
   :width: 30%

   Ahí aparecerán los subconductos que vayamos creando, según el conducto preseleccionado.



.. figure::  images/03_funcionalidades/image171.png
   :align:   center
   :width: 30%

   Representación de un subconducto

.. |image114| image:: images/03_funcionalidades/image114.png
    :width: 100

**iv. Eliminar subconducto**
    Para eliminar un subconducto, tan solo debemos seleccionarlo haciendo clic en el SUBCOND que queramos eliminar y darle al botón |image114|.

**v. Esquema**
    Una vez realizados los conductos y subconductos, si vamos a la pestaña Esquemas, podemos verlo gráficamente.

.. figure::  images/03_funcionalidades/image30.png
   :align:   center
   :width: 30%

   Vista general de canalización


c. Equipos
----------

Desde la ventana de equipos, podemos añadir bandejas, splitters y patch pannels.

.. figure::  images/03_funcionalidades/image40.png
   :align:   center
   :width: 40%


.. |image167| image:: images/03_funcionalidades/image167.png
    :width: 80

**i. Bandejas**
    Para ello, desplegamos la pestaña bandejas. Seleccionamos la capacidad que deseemos y le damos al botón |image167|.

    Una vez hecho esto, nos aparecerá a la derecha de Nº Bandejas: BAND 1

.. figure::  images/03_funcionalidades/image156.png
   :align:   center
   :width: 40%

   Apareceren las bandejas que vamos creando


    .. |image68| image:: images/03_funcionalidades/image68.png
       :width: 80

   Para eliminar una bandeja seleccionamos el BAND y le damos al botón |image68|

.. figure::  images/03_funcionalidades/image43.png
   :align:   center
   :width: 30%

   Esquema de la bandeja


.. |image42| image:: images/03_funcionalidades/image42.png
    :width: 80

**ii. Divisores**
    Para ello, desplegamos la pestaña divisores. Seleccionamos las entradas y las salidas que deseemos y le damos al botón |image42|.


    Una vez hecho esto nos aparecerá a la derecha de Nº Splitters: DV 1


.. figure::  images/03_funcionalidades/image160.png
   :align:   center
   :width: 40%

   Apareceren los divisores que vamos creando

    .. |image73| image:: images/03_funcionalidades/image73.png
        :width: 80


    Para eliminar un divisor seleccionamos el DV y le damos al botón |image73|.

.. figure::  images/03_funcionalidades/image180.png
   :align:   center
   :width: 30%

   Esquema del splitter


.. |image178| image:: images/03_funcionalidades/image178.png
    :width: 100

**iii. Patchs**
    Para ello, desplegamos la pestaña patch. Seleccionamos la capacidad y el rótulo que deseemos y le damos al botón |image178|.


    Una vez hecho esto nos aparecerá a la derecha de Nº Patchs: PATCH 1

.. figure::  images/03_funcionalidades/image80.png
   :align:   center
   :width: 40%

   Apareceren los patchs que vamos creando

    .. |image107| image:: images/03_funcionalidades/image107.png
        :width: 100


    Para eliminar un patch, seleccionamos el PATCH y le damos al botón |image107|.


.. figure::  images/03_funcionalidades/image161.png
   :align:   center
   :width: 30%

   Esquema del patch pannel

d. Exportación de esquemas
--------------------------

Los esquemas de los diferentes elementos se pueden exportar en el formato de imagen png.

.. |image105| image:: images/03_funcionalidades/image105.png
    :width: 25

Simplemente, debemos hacer clic en el botón de exportar |image105|


.. figure::  images/03_funcionalidades/image75.png
   :align:   center

.. figure::  images/03_funcionalidades/image106.png
   :align:   center

e. Propagación
--------------

Esta herramienta la analizaremos más detalladamente en lógica de red.

En resumen, la propagación nos permite resaltar el camino lógico de  continuidad de una fibra, tanto en el esquema de una bandeja como en el de una fibra. Simplemente, pulsamos sobre la fibra o el terminal de uno de los esquemas.

.. figure::  images/03_funcionalidades/image129.png
   :align:   center
   :width: 80%

Como vemos en la imagen, se muestra el recorrido de la propagación sombreado en amarillo después de haber seleccionado la posición 3 del segmento.


Tabla de Atributos
==================

La tabla de atributos muestra  las distintas capas del mapa

.. figure::  images/03_funcionalidades/image44.png
   :align:   center
   :width: 80%

.. |image204| image:: images/03_funcionalidades/image204.png
    :width: 25

.. |image205| image:: images/03_funcionalidades/image205.png
    :width: 25

.. |image206| image:: images/03_funcionalidades/image206.png
    :width: 25

.. |image207| image:: images/03_funcionalidades/image207.png
    :width: 25

.. |image208| image:: images/03_funcionalidades/image208.png
    :width: 25

.. |image209| image:: images/03_funcionalidades/image209.png
    :width: 25

.. |image210| image:: images/03_funcionalidades/image210.png
    :width: 25

.. |image211| image:: images/03_funcionalidades/image211.png
    :width: 25

|image204| Mostrar en el mapa el elemento seleccionado.

|image205| Borrar el elemento seleccionado.

|image206| Restaurar datos iniciales.

|image207| Guardar cambios.

|image208| Exportar a .CSV.

|image209| Exportar a .SHP.

|image210| Exportar a .KML.

|image211| Exportar a .GPKG.

Gestor de Conexión
==================

El gestor de conexiones nos permite conectar cables con equipos.

Algo a tener en cuenta es que para que, un equipo esté conectado a un cable, deberemos entrar en el modo de edición del equipo y pegar la cruceta roja al cable, como vemos en la siguiente captura:

.. figure::  images/03_funcionalidades/image159.png
   :align:   center
   :width: 80%

Una vez posicionado correctamente, le damos a guardar para confirmar los cambios.

Para usar la herramienta gestor de conexiones, hacemos clic en Gestor Conexiones, se nos abrirá la siguiente interfaz:

.. figure::  images/03_funcionalidades/image60.png
   :align:   center
   :width: 55%

Como vemos en la imagen, la casilla origen aparece marcada en amarillo, por lo que debemos seleccionar el origen de la conexion. En el ejemplo, seleccionaremos al equipo haciendo clic sobre él.

Una vez seleccionado el equipo, aparecerán las conexiones posibles que anteriormente habremos configurado en el mismo. Ahora, debemos hacer clic en la casilla Destino para que se ponga en amarillo y, posteriormente, seleccionamos el destino haciendo clic en él, sobre el mapa. Una vez hecho, se cargan sus posibles conexiones.

.. figure::  images/03_funcionalidades/image101.png
   :align:   center
   :width: 40%

   Origen seleccionado


.. figure::  images/03_funcionalidades/image109.png
   :align:   center
   :width: 40%

   Destino seleccionado


Para realizar la conexión, debemos seleccionar las conexiones que queremos que se unan, para seleccionar varias al mismo tiempo, dejaremos pulsada la tecla CTRL mientras hacemos clic en ellas.

Hecho esto le daremos al botón verde para unirlas o al botón rojo si queremos eliminarlas.

.. figure::  images/03_funcionalidades/image85.png
   :align:   center
   :width: 40%

   Antes de la conexión


.. figure::  images/03_funcionalidades/image112.png
   :align:   center
   :width: 45%

   Después de la conexión

Cuando realizamos conexiones, éstas se ven reflejadas en las ventanas de información de los elementos implicados en forma de diagramas dentro del Esquema.

.. figure::  images/03_funcionalidades/image72.png
   :align:   center
   :width: 40%

   Esquema de equipo


.. figure::  images/03_funcionalidades/image32.png
   :align:   center
   :width: 50%

   Esquema de cable

Con lo que hemos realizado tan solo hemos conectado uno de los extremos del cable, por lo tanto, debemos realizar el mismo proceso desde el otro extremo. Digamos que cuando seleccionamos un elemento desde la casilla de ORIGEN, estamos seleccionando un extremo, y si seleccionamos el mismo elemento desde la casilla DESTINO, estamos seleccionando el otro extremo. Podemos verlo más sencillo con el siguiente esquema:

.. figure::  images/03_funcionalidades/image47.png
   :align:   center

En el dibujo, se muestran el origen (morado) y el destino (naranja) de los diferentes elementos. El rectángulo verde, nos muestra lo que hemos configurado. Para llegar al equipo Y, debemos seleccionar en la casilla ORIGEN el tramo x y en la casilla DESTINO el equipo Y, realizamos las conexiones que deseemos y ya tendríamos conectado el equipo x al equipo Y

Si vamos otra vez al esquema del cable, encontramos que ya se encuentra conectado por los dos extremos.

.. figure::  images/03_funcionalidades/image45.png
   :align:   center
   :width: 50%


Gestor de Asociaciones
======================

Esta herramienta nos permite atribuir cables a canalizaciones.

Para ello, abrimos la herramienta y hacemos clic primero en el cable y luego en la canalización. Ahora hacemos clic en el conducto por el que queremos que pase nuestro cable y le damos al botón verde para aplicar o al botón rojo para eliminar.

.. figure::  images/03_funcionalidades/image174.png
   :align:   center
   :width: 50%

Esta información se puede comprobar de nuevo yendo al Esquema de la canalización, donde al igual que en el gestor de asociaciones aparecerá en blanco por donde pase un cable.

Herramientas Avanzadas
======================
Cortar segmentos y canalizaciones
---------------------------------

Para cortar un cable/canalización debemos usar esta herramienta. Para explicarlo, usaremos un cable (en una canalzaición funciona igual). Simplemente, pulsamos el botón y seleccionamos, como podemos ver en la captura, dos puntos: el primero con un clic y el segundo con doble clic.

.. figure::  images/03_funcionalidades/image131.png
   :align:   center
   :width: 80%

.. figure::  images/03_funcionalidades/image130.png
   :align:   center
   :width: 80%

.. figure::  images/03_funcionalidades/image132.png
   :align:   center
   :width: 80%

Ahora tenemos dos cables que se llaman igual y tienen las mismas características, sin embargo, las conexiones permanecen en los extremos por los que vienen. Esto se ve mejor con un ejemplo.

En la captura anterior, vemos que el cable tiene conexión con un equipo y éste está conectado de la siguiente manera al cable.

.. figure::  images/03_funcionalidades/image83.png
   :align:   center
   :width: 50%

Una vez lo hemos cortado, la parte izquierda del cable permanecerá como en la captura. En el caso en que estuviera ocupado también por la derecha, pasarían a estar en no conectado.

El lado derecho del cable, como no tiene nada conectado por la derecha, aparecerá de la siguiente manera.

.. figure::  images/03_funcionalidades/image52.png
   :align:   center
   :width: 50%


Editor de Estilos CSS
=====================

Esta herramienta nos permite la personalización de elementos como: cables, equipos, canalizaciones e infraestructuras pudiendo cambiar desde colores hasta iconos y asignarle condiciones.

La configuración se realiza mediante un sistema de reglas CSS y, posteriormente, mediante un sistema de condicionales en caso de que quiera darse ese estilo a un conjunto definido de elementos. Dichas reglas se guardan en el proyecto.

En caso de querer añadir estilos, tendremos que añadir toda la casuística necesaria ya que el estilo por defecto se desactiva totalmente.

Cada uno de los elementos tiene una pestaña diferente pero la interfaz es prácticamente la misma por lo que vamos a comprobar su funcionamiento en la pestaña equipos.

Al entrar, se nos muestra una previsualización del mapa, una tabla con las distintas personalizaciones y un botón de Nueva Regla.

.. figure::  images/03_funcionalidades/image119.png
   :align:   center
   :width: 80%


Para añadir una personalización nueva le damos al botón Nueva Regla y rellenamos el formulario que aparece como deseemos.

Se puede modificar el símbolo, el tamaño del símbolo,  la opacidad del símbolo, color de relleno, opacidad de relleno, color de borde, ancho de borde y opacidad de borde:

.. figure::  images/03_funcionalidades/image150.png
   :align:   center
   :width: 80%

   Configuración de CSS

.. figure::  images/03_funcionalidades/image169.png
   :align:   center
   :width: 80%

   Editor CSS

.. |image181| image:: images/03_funcionalidades/image181.png
    :width: 25

Para que funcione debemos asignarle una condición pulsando en |image181|

.. figure::  images/03_funcionalidades/image74.png
   :align:   center
   :width: 80%

La  condición depende del elemento al que estemos asignando, en el ejemplo lo que hemos configurado es que los equipos que tengan como situación FACHADA se les aplique el estilo.

.. figure::  images/03_funcionalidades/image79.png
   :align:   center
   :width: 80%

.. |image185| image:: images/03_funcionalidades/image185.png
    :width: 90

Se pueden asignar varias condiciones a un mismo estilo. Para volver usar el estilo por defecto debemos eliminar los estilos personalizados o editarlos con el botones |image185|
