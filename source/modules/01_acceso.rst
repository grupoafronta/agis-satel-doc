##############
¿Cómo Acceder?
##############


aGIS es una aplicación multiplataforma totalmente WEB, que permite el acceso mediante cualquier navegador convencional, aunque está optimizado para:

- Mozilla Firefox 66.0.5 o posterior
- Google Chrome versión 74 o posterior

Para acceder basta con ir a la URL https://fibraoptica.ast.aragon.es/ .

.. figure::  images/01_acceso/login.png
   :align:   center
   :width: 80%

   Ventana inicial de Login.