#################
Servicios Lógicos
#################

Esta pestaña sirve para crear servicios e inyectarlos por los puertos. Para crear un servicio e inyectarlo, debemos crear el servicio y asignarle un puerto (Servicios).

Además podemos crear o modificar propietarios, usuarios y equipos terminal.


Buscador de Servicios
***********

El buscador de servicios se encuentra activo con la visualización del mapa base. Aparece en el menú lateral dentro de Servicios Lógicos.

.. figure::  images/04_servicios/image_servicios_16.png
   :align:   center
   :width: 40%

Una vez clicado en el se abre una ventana en el mapa en la cual escribimos el nombre del servicio a buscar.

.. figure::  images/04_servicios/image_servicios_17.png
   :align:   center
   :width: 80%

Una vez realizada la búsqueda se iluminará el servico y a su vez se abrirá una ventana de información de dicho servicio.

.. figure::  images/04_servicios/image_servicios_18.png
   :align:   center
   :width: 80%


Servicios
*********

Esta herramienta nos permite añadir, editar o borrar un circuito.

Como vemos en la interfaz, tenemos el botón para añadir circuitos, una tabla que representa los distintos circuitos y nos permite mediante botones en la columna acción: ver (muestra en el mapa la localización del servicio), editar o eliminar. Ésta tabla se puede exportar a los distintos formatos como se puede apreciar en la imagen.


.. figure::  images/04_servicios/image_servicios_1.png
   :align:   center
   :width: 80%


En el formulario, para añadir un nuevo circuito, tenemos los siguientes campos: nombre, usuario, equipo terminal, equipo origen, tipo de servicio, ancho de banda y aplicación. Entre los tipos de servicio encontramos transporte, monitorización, fibra oscura, gestión y ethernet.

.. figure::  images/04_servicios/image_servicios_2.png
   :align:   center
   :width: 80%

Una vez creado, si le damos al botón del lápiz, podemos editar los campos del formulario anterior y ver o añadir los puertos inyectados de los patch o de las bandejas.

.. figure::  images/04_servicios/image_servicios_3.png
   :align:   center
   :width: 80%


Para añadir un puerto le damos a añadir y rellenamos el formulario.

.. figure::  images/04_servicios/image_servicios_4.png
   :align:   center
   :width: 80%

Como vemos en la captura, en la parte izquierda tenemos los campos: nombre, conector (ST, LC, SC, APC), destino: equipo y destino: puerto. En la parte derecha se muestra información de los puertos asignables.

.. figure::  images/04_servicios/image_servicios_5.png
   :align:   center
   :width: 80%

Esta parte se encuentra justo debajo de la anterior y nos permite como vemos en la siguiente captura: desasignar (4) puertos inyectados, seleccionar un tipo de equipo (podemos elegir entre repartidores, cajas de empalme y torpedos), seleccionar un equipo (1) (haciendo clic sobre él), seleccionar un subequipo (2) y por último seleccionar un puerto (3).

En conclusión, tenemos que realizar los pasos del 1 al 3 para inyectar un servicio en un puerto.

.. figure::  images/04_servicios/image_servicios_6.png
   :align:   center
   :width: 80%

Una vez tenemos inyectado el servicio, podemos comprobar su propagación.


Propagación
***********

La propagación de un servicio es ver, de forma gráfica, desde donde hasta donde va un servicio, es decir, ver el equipo origen y el equipo destino. aGIS es una herramienta muy poderosa en la cual se puede ver la propagación de los servicios de forma muy fácil e intuitiva. Para ver la propagación de un servicio, se debe colocar en el mapa sobre el equipo origen del servicio y hacer clic sobre él. Al realizar el paso previo, aparecerá una ventana con información en la cual se pueden ver los Patch Pannel del equipo:

.. figure::  images/04_servicios/image_servicios_7.png
   :align:   center
   :width: 50%

Los patch pannels ahora son inteligentes, a pesar de tener dos extremos, solo se representa el extremo disponible al que no se ha conectado cable, y todo esto de forma automática y transparente para el usuario.

Se han optimizado todos los procesos de consultas y velocidad, para todos los algoritmos de propagación y representación de orígenes y destinos.

Como se observa en la imagen anterior, el dispositivo para el ejemplo posee un Patch Pannel. Se debe elegir el Patch Pannel origen de la fibra que posea el servicio, y acceder a él haciendo clic sobre él. Aparecerá un nuevo menú sobre el que se pulsará “Ver Servicios Patch” para indicar que fibras poseen servicios activos.

.. figure::  images/04_servicios/image_servicios_8.png
   :align:   center
   :width: 50%

Ahora las fibras con servicios activos se representarán en color verde, las fibras activas pero sin servicios se representan en color azul, y las fibras no activas se representan en color rojo.

Para ver el trayecto de un servicio, solo se debe clicar sobre la fibra con el servicio activo y automáticamente esta fibra se resaltará en el mapa para facilitar su visualización.

.. figure::  images/04_servicios/image_servicios_9.png
   :align:   center
   :width: 80%



Propietarios
************

Al entrar dentro de la opción Propietarios, se puede editar los propietarios de los servicios ya creados o bien crear nuevos propietarios.

.. figure::  images/04_servicios/image_servicios_10.png
   :align:   center
   :width: 80%

Para crear nuevos Propietarios, se debe hacer clic sobre “Añadir” y rellenar los campos que se solicitan.

.. figure::  images/04_servicios/image_servicios_11.png
   :align:   center
   :width: 80%

   Formulario propietarios

Usuarios
********

En la pestaña de Usuarios, se indican todos los Usuarios que usan los servicios especificados.

.. figure::  images/04_servicios/image_servicios_12.png
   :align:   center
   :width: 80%

Para añadir nuevos usuarios, se ha de pulsar sobre el botón “Añadir” y rellenar los datos que se solicitan.

.. figure::  images/04_servicios/image_servicios_13.png
   :align:   center
   :width: 80%

   Formulario usuarios

Los usuarios registrados no tienen porque tener servicios asignados.


Equipo Terminal
***************

Al entrar dentro de Equipo Terminal, se pueden editar los equipos ya creados o bien crear nuevos equipos.

.. figure::  images/04_servicios/image_servicios_14.png
   :align:   center
   :width: 80%

Para añadir nuevos equipos terminal, se ha de pulsar sobre el botón “Añadir” y rellenar los datos que se solicitan.

.. figure::  images/04_servicios/image_servicios_15.png
   :align:   center
   :width: 80%

   Formulario equipo terminal